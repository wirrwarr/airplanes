library airplane;

import "dart:html";
import "dart:math";
import "dart:async";


main() {
  var canvas = (query("#battlefield") as CanvasElement);
  var width = canvas.width;
  var height = canvas.height;
  var tmp = canvas.context2d;
  var airport = new Airport("Frankfurt", canvas);
  runAsync(airport.start);
  query("#airport_name").text += airport.name;

}

var notes = query("#fps");
var fpsAverage;

reloadPage(e) {
  window.location.reload();
}

showFps(fps) {
  if (fpsAverage == null) fpsAverage = fps;
  fpsAverage = fps * 0.05 + fpsAverage * 0.95;
  notes.text = "${fpsAverage.round()} fps";
}

showAirplanes(airplanes) {
  var tableBody = query("table tbody");
  tableBody.innerHtml = "";
  airplanes.sort((a,b) => a.buildYear.compareTo(b.buildYear));
  airplanes.forEach((a) {
      var tableRow = new TableRowElement();
      tableRow.classes.add("uncatched");
      if (a.isLanded) tableRow.classes.add("catched");
      tableRow.children.add(
        new TableCellElement()..text = a.id 
      );
      tableRow.children.add(
        new TableCellElement()..text = a.painting.toString()
      );
      tableBody.children.add(tableRow);
  });

}

showAirportInfos(allAirplanes) {
  var a = query("#quantity_airplanes");
  a.text =  allAirplanes.toString();
}

showPoints(points) {
  var e = query("#game_points");
  e.text = points.toString();
}

createMathExercise() {
  var r = new Random();
  var v1 = r.nextInt(10);
  var v2 = r.nextInt(10);
  var s = v1.toString() + " + " + v2.toString();

  return {"term": s, "solution": v1+v2};
}

calcRadian(degress) {
  return degress*(PI/180);
}

class Point {

  var radius;
  var borderColor;
  var fillColor;
  var x;
  var y;

  Point(this.radius, this.borderColor, this.fillColor);

  draw(canvas, x, y) {
    this.x = x;
    this.y = y;
    canvas..beginPath()
          ..lineWidth = 2
          ..fillStyle = fillColor 
          ..strokeStyle = borderColor 
          ..arc(x, y, radius, 0, PI*2, false)
          ..fill()
          ..closePath()
          ..stroke();

  }
}

class Airplane {

  var id;
  var landingCode;
  var painting;
  var points;
  var flightPath;
  var airplaneColor = "#0ca1e3";
  var renderTime;
  var distanceToAirport;
  var _isLanded = false; 
  var speed;
  var canvas;
  var startPointX;
  var startPointY;
  var cWidth;
  var cHeight;
  var buildYear;

  Airplane(this.id, this.flightPath, this.speed, this.canvas) {
    distanceToAirport = startDistanceToAirport();
    cWidth = canvas.canvas.width;
    cHeight = canvas.canvas.height;
    buildYear = new DateTime.now().millisecondsSinceEpoch;
    tagAirplane();
  }

  get isLanded => _isLanded;
  set isLanded(b) => _isLanded = b;

  tagAirplane() {
    var mathExer = createMathExercise();
    landingCode = mathExer["solution"];
    painting = mathExer["term"];
  }

  startDistanceToAirport() {
    return this.canvas.canvas.width/2;
  }

  draw() {
    var time = new DateTime.now().millisecondsSinceEpoch;
    if (renderTime == null) {
      renderTime = time;
      return;
    }

    if (isLanded) {
      airplaneColor = "#0bb52f";
    }

    var radius = distanceToAirport;
    var degress = flightPath;
    var airplaneLengthPropo = 0.04;
    var airplaneTipDeg = 20;

    var airplaneLength = cWidth*airplaneLengthPropo;

    startPointX = cWidth/2+(cos(calcRadian(degress))*radius);
    startPointY = cHeight/2-(sin(calcRadian(degress))*radius);
    var wingPointSouthX = startPointX+(cos(calcRadian(airplaneTipDeg-degress))*airplaneLength);
    var wingPointSouthY = startPointY+(sin(calcRadian(airplaneTipDeg-degress))*airplaneLength);
    var wingPointNorthY = startPointY+(sin(calcRadian(airplaneTipDeg+degress))*airplaneLength*-1);
    var wingPointNorthX = startPointX+(cos(calcRadian(airplaneTipDeg+degress))*airplaneLength);
    var textX = wingPointNorthX;
    var textY = wingPointNorthY-30;

    /*
    print("startPointX: ${startPointX}");
    print("startPointY: ${startPointY}");
    print("wingPointSouhtX: ${wingPointSouthX}");
    print("wingPointSouhtY: ${wingPointSouthY}");
    print("wingPointNorthX: ${wingPointNorthX}");
    print("wingPointNorthY: ${wingPointNorthY}");
    print("textX: ${textX}");
    print("textY: ${textY}");
    */

    drawFlightPath();

    canvas..beginPath()
          ..lineWidth = 2
          ..strokeStyle = airplaneColor 
          ..fillStyle = "#bac0c3"
          ..moveTo(startPointX, startPointY)
          ..lineTo(wingPointNorthX, wingPointNorthY)
          ..moveTo(startPointX, startPointY)
          ..lineTo(wingPointSouthX, wingPointSouthY)
          ..lineTo(wingPointNorthX, wingPointNorthY)
          ..fill()
          ..font = "italic 12pt Calibri"
          ..fillStyle = "#646464"
          ..fillText("*ID: ${painting}*", textX, textY)
          ..closePath()
          ..stroke();

    distanceToAirport = distanceToAirport - (time-renderTime)*speed;
    renderTime = time;

  }

  drawFlightPath() {
    canvas..beginPath()
          ..lineWidth = 1
          ..strokeStyle = "#9ba7ad" 
          ..moveTo(startPointX+(cos(calcRadian(flightPath))*400), startPointY-(sin(calcRadian(flightPath))*400))
          ..lineTo(cWidth/2, cHeight/2)
          ..closePath()
          ..stroke();
  }

}


class Airport {

  var name;
  var airplanes = [];
  var zone1;
  var zone2;
  var zone3; 
  var canvas;
  var renderTime;
  var cWidth;
  var cHeight;
  var pos = 200;
  var stopTime = 1550;
  var stopTimeTmp = 0;
  var quantityAirplanes = 10;
  var theWholeShit = 0;
  var keyBuffer = [];
  var points = 0;
  var startSpeed = 0.02;

  Airport(this.name, this.canvas) {
    cWidth = canvas.width;
    cHeight = canvas.height;
  }

  start() {
    window.onKeyPress.listen(catchKey);
    query("#reload").onClick.listen(reloadPage);
    requestRedraw();
  }

  calPoints(distanceToAirport) {
    var points;
    if (distanceToAirport > zone1.radius) {
      points = 6;
    } else if (distanceToAirport > zone2.radius) {
      points = 4;
    } else if (distanceToAirport > zone3.radius) {
      points = 2;
    }

    return points;
  }

  catchAirplane(landingCode) {
    airplanes.forEach((a) {
      if (a.landingCode == landingCode) {
        a.isLanded = true;
        points += calPoints(a.distanceToAirport);
        showAirplanes(airplanes);
        showPoints(points);
      }
    });
  }
 

  catchKey(e) {
    if (e.charCode == 13) {
      catchAirplane(calcNumber(keyBuffer));
      keyBuffer = [];
    } else if (e.charCode == 27) {
      keyBuffer = [];
    } else {
      keyBuffer.add(e.charCode);
    }
    requestRedraw();
  }

  calcNumber(buffer) {
    var tmp = 0;
    var numbers = buffer.where((e) => (((e-48) >= 0) && ((e-48) <= 9))).map((e) => (e-48)).toList();
    for (var x = 0; x < numbers.length; x++) {
        tmp += numbers[x]*pow(10, numbers.length-x-1);     
    }
    print(tmp);
    return tmp;
    
  }
  
  drawBuildings(world) {
    var radius_propo = 0.30;
    var radius = cWidth * radius_propo;
    var inner_cycle_propo = 0.12;
    var inner_cycle_gap = cWidth * inner_cycle_propo;

    world.clearRect(0, 0, cWidth, cHeight);

    zone1 = new Point(radius, "#1ba036", "#8abc3f");
    zone1.draw(world, cWidth/2, cHeight/2);
    zone2 = new Point(radius-inner_cycle_gap, "#ffcc00", "#ffe479");
    zone2.draw(world, cWidth/2, cHeight/2);
    zone3 = new Point(radius-2*inner_cycle_gap, "#9c0000", "#c72a2a");
    zone3.draw(world, cWidth/2, cHeight/2);

    world..beginPath()
         ..lineWidth = 1 
         ..strokeStyle = "#bac0c3" 
         ..moveTo(0, cHeight/2)
         ..lineTo(cWidth, cHeight/2)
         ..moveTo(cWidth/2, 0)
         ..lineTo(cWidth/2, cHeight)
         ..closePath()
         ..stroke();

  }

  calcSpeed() {
    return startSpeed;
  }

  draw(_) {
    var time = new DateTime.now().millisecondsSinceEpoch;
    if (renderTime != null) {
      showFps(1000 / (time - renderTime));

      var world = canvas.context2d;
      drawBuildings(world);

      if (stopTimeTmp > stopTime) { 
        if (airplanes.length < quantityAirplanes) {
          var flightPath = new Random().nextInt(360);
          airplanes.add(new Airplane("1", flightPath, calcSpeed(), world));
          theWholeShit += 1;
          showAirplanes(airplanes);
          showAirportInfos(theWholeShit);
        }
        stopTimeTmp = 0;
      }

      for (var x = 0; x < airplanes.length; x++) {
        if (airplanes[x].distanceToAirport < zone3.radius/2) {
          if (!airplanes[x].isLanded) {
            return -1;
          }
          airplanes.removeAt(x);
        }
      }
      
      airplanes.forEach((e) { 
          e.draw();
      });
      stopTimeTmp = stopTimeTmp+(time-renderTime);
    }

    renderTime = time;
    if (points >= 0) {
      requestRedraw();
    }
  }

  requestRedraw() {
    window.requestAnimationFrame(draw);
  }


}

